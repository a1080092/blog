---
weight: 1
title: "【台湾料理】餃子の皮で焼きネギ餅を作ってみましょう！"
date: 2022-01-04T15:00:00+08:00
lastmod: 2022-01-04T15:00:00+08:00
description: "スーパーで簡単に買える餃子の皮でネギ焼き餅を作りましょう！"
resources:
- name: "featured-image"
  src: "featured-image.jpeg"

tags: ["台湾料理", "焼きネギ餅"]
categories: ["レシピ"]


---
スーパーで簡単に買える餃子の皮で人気なグルメの焼きネギ餅を作るレシピです！

<!--more-->
{{< admonition >}}
✎ 台湾では、鉄板を使って「ネギ入り焼き餅」を作っている屋台をよく目にするでしょう👀！朝ごはんとか、3時のおやつ🧇とか、夜食とかで、よく食べられるネギ餅は外がサクサク、中は柔らかくて美味しいです✨！中に入ってるネギは油で焼いたら、香ばしい香りが引き立つので、いつも無意識に食べ過ぎてしまうグルメです😗！

自分で作りたい！しかし、生地から作るのは手間暇がかかるから、考えるだけ諦めたくなります😩！だから、今回は家によくあり、スーパーで簡単に買える「餃子の皮」で作ってみました！皮を何枚も使うので、層がいっぱいあり、生地で作ったネギ餅に負けない美味しさがあります。ぜひ、作ってみましょう！👌🏻✨
{{< /admonition >}} 

### 👩‍🍳 材料
- 餃子の皮 … 好きな枚数 (４〜7枚はおすすめです）
- ネギ (緑の部分) … 適量
- 油 (動物性・植物性) … 適量
- 調味料 (ホワイトペッパー・塩)… 適量

{{< admonition tip>}}
餃子の皮はお好みでネギ餅のサイズにより、決めます！よく7枚で作りますが、少量だけ食べたい人は5枚でも構いません。 
{{< /admonition >}}

{{< admonition warning>}}
⚠️ 巻く時、生地が破れやすいので、ネギを細く刻めば刻むほど良いです。  
⚠️ 豚脂🐷など動物性の油がおすすめですが、ないなら普通の植物油を使ってもいいです。  
⚠️ ホワイトペッパーがあれば、ぜひ入れてください。  
{{< /admonition >}}

### 👩‍🍳 作り方
① 豚脂を電子レンジで10秒くらい温め、調味料と混ぜます。  
![Minion](./image/01.png)

② 餃子の皮を六枚準備します。油は皮を伸ばすとき、皮から溢れないように気をつけましょう。皮の縁まで塗らないで少し空けておき、真ん中に油を塗ります。  
③ 刻んだネギを少量餃子の皮の中心に載せ、全部の皮を重ねます。
![Minion](./image/02.png)

{{< admonition warning>}}
⚠️ ネギを大量に載せたら、皮を巻く時に破れてしまいますので、少量だけ載せてください。
{{< /admonition >}}

④ 残った一枚の餃子の皮を上に載せ、手で生地を薄く均し、伸ばした時ネギが出てこないように、フォークで縁の部分をしっかり押します。  
![Minion](./image/03.png)

⑤ 生地を綺麗に丸く伸ばし、上から下に向かって巻きます(1回目)
⑥ もう一度手で巻き終わった生地を薄く均し、左から右へ、違う向きで巻きます。接着部分は生地の下に置き(2回目)、生地の繋がりが弱くなるように、そのまま十分くらい放置します。  
![Minion](./image/04.png)

{{< admonition warning>}}
⚠️ 接着部は生地の下に置かなければ、伸ばす時形が崩れやすくなります！
{{< /admonition >}}

⑦ 手で生地を薄く均し、麺棒で好きな厚さに伸ばします
![Minion](./image/05.png)
{{< admonition warning>}}
⚠️ 力強く伸ばすと、縁は破れやすいです  
⚠️ 破れたら、気にしないで続けてします
{{< /admonition >}}

### 👩‍🍳 保存食の作り方
一、一週間以内で食べる場合  
![Minion](./image/06.jpeg)
➊ 作り方のステップー⑥が終わったら、そのまま冷蔵庫で保存します。  
❷ 食べたい時生地を一つとり、作り方のステップー⑦から続けます。


二、長期保存したい場合  
➊ 作り方のステップー⑦までし、生地がお互いにくっつかないように、伸ばした生地を袋で包み、冷凍します。  
❷ 食べたい時に解凍しないで、冷凍の状態で直接焼きます。三ヶ月くらい保存できます。



